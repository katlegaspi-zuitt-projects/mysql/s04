-- Find all artists that has letter D in its name.
SELECT *
FROM artists
WHERE name LIKE "%d%";

-- Find all songs that has a length of less than 230.
SELECT *
FROM songs
WHERE length < 230;

-- Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)
SELECT albums.name AS album_name, songs.title AS song_title, songs.length AS song_length
FROM albums JOIN songs ON albums.id = songs.album_id;


-- Join the 'artists' and 'albums' tables. (Find all albums that has letter A in its name.)
SELECT artists.name AS artist_name, albums.name AS album_name
FROM artists JOIN albums ON artists.id = albums.artist_id
WHERE albums.name LIKE "%a%";


-- Sort the albums in Z-A order. (Show only the first 4 records.)
SELECT *
FROM albums
ORDER BY name DESC
LIMIT 4;

-- Join the 'albums' and 'songs' tables. (Sort albums from Z-A and sort songs from A-Z.)
SELECT albums.name AS album_name, songs.title AS song_title
FROM albums JOIN songs ON albums.id = songs.album_id
ORDER BY album_name DESC, song_title ASC;
