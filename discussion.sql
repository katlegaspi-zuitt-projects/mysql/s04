-- Add the following records in artists table:

-- Name
-- ----
-- Taylor Swift
-- Lady Gaga
-- Justin Bieber
-- Ariana Grande
-- Bruno Mars

INSERT INTO artists (name)
VALUES ("Taylor Swift"),
("Lady Gaga"),
("Justin Bieber"),
("Ariana Grande"),
("Bruno Mars");

-- Add the following records in albums table:

-- Name                Year        Artist
-- --------------------------------------
-- Fearless              2008        Taylor Swift
-- Red                   2012        Taylor Swift
-- A Star Is Born        2018        Lady Gaga
-- Born This Way         2011        Lady Gaga
-- Purpose               2015        Justin Bieber
-- Believe               2012        Justin Bieber
-- Dangerous Woman       2016        Ariana Grande
-- Thank U, Next         2019        Ariana Grande
-- 24K Magic             2016        Bruno Mars
-- Earth to Mars         2011        Bruno Mars

INSERT INTO albums (name, year, artist_id)
VALUES ("Fearless", "2008-1-1", 3),
("Red", "2012-1-1", 3),
("A Star Is Born", "2018-1-1", 4),
("Born This Way", "2011-1-1", 4),
("Purpose", "2015-1-1", 5),
("Believe", "2012-1-1", 5),
("Dangerous Woman", "2016-1-1", 6),
("Thank U, Next", "2019-1-1", 6),
("24K Magic", "2016-1-1", 7),
("Earth to Mars", "2011-1-1", 7);

Add the following records in the songs table:

INSERT INTO songs (title, length, genre, album_id)
VALUES ("Fearless", "246", "Pop Rock", 13),
("Love Story", "213", "Country Pop", 13),
("State of Grace", "313", "Alternative Rock", 14),
("Red", "204", "Country", 14),
("Black Eyes", "181", "Rock n Roll", 15),
("Shallow", "201", "Country, Rock, Folk Rock", 15),
("Born This Way", "256", "Electropop", 16),
("Sorry", "256", "Dancehall-poptropical housemoombahton", 17),
("Boyfriend", "256", "Pop", 18),
("Into You", "256", "EDM House", 19),
("Thank U, Next", "256", "Pop, R&B", 20),
("24K Magic", "256", "Funk, Disco, R&B", 21),
("Lost", "256", "Pop", 22);

-- Advanced Select Statements

-- Exclude records upon retrieving
-- SELECT <column>[, <column>]
-- FROM <table>
-- WHERE <condition>;

-- Retrieve records from songs table where length is not 0:00
SELECT *
FROM songs
WHERE length != 0;

-- Greater than or equal to
-- Retrieve records from the songs table where length is greater than 2:30.
SELECT *
FROM songs
WHERE length > 230;

-- Retrieve records from the songs table where length is less than 2:00.
SELECT *
FROM songs
WHERE length < 200;

-- Get specific records using OR
-- Retrieve records from songs table where id is equal to 1 or 3 or 5.
SELECT *
FROM songs
WHERE id = 1
OR id = 3
OR id = 5;

-- Get specific records using IN
-- Retrieve records from songs table where id is equal to 1 or 3 or 5.
SELECT *
FROM SONGS
WHERE id IN (1, 3, 5);

-- Combining conditions using AND
-- Retrieve records from songs where album_id is equal to 4 and id is less than 8.
SELECT *
FROM songs
WHERE album_id = 4
AND id < 8;

-- Find using partial matches
-- The keyword is found at the end
-- Retrieve records from the songs table where the title ends in 'y'.
SELECT title
FROM songs
WHERE title LIKE "%y";

-- The keyword is found from the start
-- Retrieve records from the songs table where the title starts in 's'.
SELECT title
FROM songs
WHERE title LIKE "s%";

-- The keyword is found in between
-- Retrieve records from the songs table where the title 
SELECT title
FROM songs
WHERE title LIKE "%u%";
--
SELECT title
FROM songs
WHERE title LIKE "%TO%";

-- We know some keyword
-- Get specific pattern
-- Retrieve the records from albums table where year released was in the 1990's.
SELECT *
FROM albums
WHERE year LIKE "199_%";

-- Retrieve the records from albums table where year released was in the 2000's.
SELECT *
FROM albums
WHERE year LIKE "20__%";
---
SELECT *
FROM albums
WHERE year LIKE "20_2%";

-- Sorting records: ascending (ASC) or descending (DESC)
SELECT *
FROM songs
ORDER BY title ASC;
--
SELECT *
FROM songs
ORDER BY title DESC;

-- Retrieve distinct records
SELECT DISTINCT genre FROM songs;
SELECT DISTINCT genre FROM songs ORDER BY genre ASC;

-- Joining tables
-- Retrieve all records from the artists and albums tables
SELECT *
FROM artists JOIN albums
ON artists.id = albums.artist_id;
--
SELECT *
FROM artists
JOIN albums ON artists.id = albums.artist_id
JOIN songs ON albums.id = songs.album_id;

-- Return specific columns from the two tables.
SELECT artists.name, albums.name
FROM artists JOIN albums
ON artists.id = albums.artist_id;
--
SELECT artists.name AS artist_name, albums.name AS album_name
FROM artists JOIN albums
ON artists.id = albums.artist_id;
--
SELECT ar.name AS artist_name, al.name AS album_name
FROM artists AS ar JOIN albums AS al
ON ar.id = al.artist_id;